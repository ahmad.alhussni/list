<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('image')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('token')->nullable();
            $table->string('code')->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('note')->nullable();
            $table->boolean('status')->nullable()->default(1);
            $table->rememberToken();
            $table->timestamps();
        });

        User::insert(['first_name' => 'Admin' ,'last_name' => 'Panel' , 'mobile' => '970599883621' , 'email' => 'admin@email.com', 'email_verified_at' => date('Y-m-d H:i:s') ,'password' => Hash::make('123456'),'created_at' => date('Y-m-d H:i:s')]);

        $user = User::find(1);

        $Admin = Role::findOrCreate('Admin');
        $Furniture = Role::findOrCreate('Furniture');
        $Designers = Role::findOrCreate('Designers');
        $Contractors = Role::findOrCreate('Contractors');
        $Customers = Role::findOrCreate('Customers');

        $user->syncRoles($Admin->id);
        foreach (User::perms() as $permission) {
            Permission::findOrCreate($permission);
            $Admin->givePermissionTo($permission);
        }

        $Furniture->givePermissionTo("products management");
        $Furniture->givePermissionTo("sliders management");
        $Furniture->givePermissionTo("banners management");
        $Contractors->givePermissionTo("projects management");
        $Contractors->givePermissionTo("consultancies management");
        $Contractors->givePermissionTo("sliders management");
        $Contractors->givePermissionTo("banners management");
        $Designers->givePermissionTo("projects management");
        $Designers->givePermissionTo("consultancies management");
        $Designers->givePermissionTo("sliders management");
        $Designers->givePermissionTo("banners management");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
