<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->float('price');
            $table->bigInteger('category_id');
            $table->boolean('new')->nullable()->default(0);
            $table->boolean('best_seller')->nullable()->default(0);
            $table->boolean('best_price')->nullable()->default(0);
            $table->string('color')->nullable();
            $table->json('material_id')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->bigInteger('user_id')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
