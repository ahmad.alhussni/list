<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*
         * Table: rates
         */
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->string('comment')->nullable();
            $table->string('image')->nullable();
            $table->integer('status')->default('1');
            $table->bigInteger('user_id')->nullable();
            $table->morphs('parent');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rates');
    }
}
