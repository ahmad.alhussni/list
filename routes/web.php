<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['lang'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {

        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginPost')->name('loginPost');
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::group(['middleware' => ['auth', 'AdminCheck']], function () {

            Route::get('/', 'DashboardController@dashboard')->name('home');
            Route::get('settings', 'DashboardController@settings')->name('settings');
            Route::post('settings', 'DashboardController@settingsPost');

            Route::resources([
                'users' => 'UserController',
                'roles' => 'RoleController',
                'sliders' => 'SliderController',
                'tags' => 'TagController',
                'categories' => 'CategoryController',
                'blogs' => 'BlogController',
                'pages' => 'PageController',
                'banners' => 'BannerController',
                'services' => 'ServiceController',
                'orders' => 'OrderController',
                'partners' => 'PartnerController',
                'payments' => 'PaymentController',
                'coupons' => 'CouponController',
                'shipments' => 'ShipmentController',
                'rates' => 'RateController',
                'contacts' => 'ContactController',
                'projects' => 'ProjectController',
                'products' => 'ProductController',
                'consultancies' => 'ConsultancyController',
                'materials' => 'MaterialController',
            ]);

        });
    });
    Route::group(['namespace' => 'Furniture', 'prefix' => 'furniture', 'as' => 'furniture.'], function () {

        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginPost');
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::group(['middleware' => ['auth', 'FurnitureCheck']], function () {

            Route::get('/', 'DashboardController@dashboard')->name('home');

            Route::resources([
                'users' => 'UserController',
                'roles' => 'RoleController',
                'sliders' => 'SliderController',
                'tags' => 'TagController',
                'categories' => 'CategoryController',
                'blogs' => 'BlogController',
                'pages' => 'PageController',
                'banners' => 'BannerController',
                'services' => 'ServiceController',
                'orders' => 'OrderController',
                'partners' => 'PartnerController',
                'payments' => 'PaymentController',
                'coupons' => 'CouponController',
                'shipments' => 'ShipmentController',
                'rates' => 'RateController',
                'contacts' => 'ContactController',
                'projects' => 'ProjectController',
                'products' => 'ProductController',
                'consultancies' => 'ConsultancyController',
                'materials' => 'MaterialController',
            ]);

        });
    });
    Route::group(['namespace' => 'Contractor', 'prefix' => 'contractor', 'as' => 'contractor.'], function () {

        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginPost');
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::group(['middleware' => ['auth', 'ContractorCheck']], function () {

            Route::get('/', 'DashboardController@dashboard')->name('home');

            Route::resources([
                'users' => 'UserController',
                'roles' => 'RoleController',
                'sliders' => 'SliderController',
                'tags' => 'TagController',
                'categories' => 'CategoryController',
                'blogs' => 'BlogController',
                'pages' => 'PageController',
                'banners' => 'BannerController',
                'services' => 'ServiceController',
                'orders' => 'OrderController',
                'partners' => 'PartnerController',
                'payments' => 'PaymentController',
                'coupons' => 'CouponController',
                'shipments' => 'ShipmentController',
                'rates' => 'RateController',
                'contacts' => 'ContactController',
                'projects' => 'ProjectController',
                'products' => 'ProductController',
                'consultancies' => 'ConsultancyController',
                'materials' => 'MaterialController',
            ]);

        });
    });
    Route::group(['namespace' => 'Designer', 'prefix' => 'designer', 'as' => 'designer.'], function () {

        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginPost');
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::group(['middleware' => ['auth', 'DesignerCheck']], function () {

            Route::get('/', 'DashboardController@dashboard')->name('home');

            Route::resources([
                'users' => 'UserController',
                'roles' => 'RoleController',
                'sliders' => 'SliderController',
                'tags' => 'TagController',
                'categories' => 'CategoryController',
                'blogs' => 'BlogController',
                'pages' => 'PageController',
                'banners' => 'BannerController',
                'services' => 'ServiceController',
                'orders' => 'OrderController',
                'partners' => 'PartnerController',
                'payments' => 'PaymentController',
                'coupons' => 'CouponController',
                'shipments' => 'ShipmentController',
                'rates' => 'RateController',
                'contacts' => 'ContactController',
                'projects' => 'ProjectController',
                'products' => 'ProductController',
                'consultancies' => 'ConsultancyController',
                'materials' => 'MaterialController',
            ]);

        });
    });
});
