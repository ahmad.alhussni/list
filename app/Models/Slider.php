<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Translation;

class Slider extends Model
{
    protected $fillable = ['image','slug','status','url','type','user_id'];
    protected $guarded = [];
    protected $appends = ['name','description'];

    public function getImageAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : "";
    }

    public function translates(){
        return $this->morphMany(Translation::class,'parent');
    }

    public function translate($key,$language)
    {
        return ($translate = $this->translates()->where('key',$key)->where('language',$language)->first()) ? $translate->value : "";
    }


    public function getNameAttribute($val)
    {
        return ($name = $this->translates()->where('key','name')->where('language',app()->getLocale())->first()) ? $name->value : "";
    }

    public function getDescriptionAttribute($val)
    {
        return ( $description = $this->translates()->where('key','description')->where('language',app()->getLocale())->first()) ? $description->value : "";
    }

}
