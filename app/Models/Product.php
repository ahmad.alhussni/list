<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Translation;

class Product extends Model
{
    protected $fillable = [
        'image',
        'name',
        'slug',
        'price',
        'category_id',
        'new',
        'best_seller',
        'best_price',
        'color',
        'materials',
        'status',
        'user_id',
    ];
    protected $guarded = [];
    protected $appends = ['name'];

    public function getImageAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : "";
    }

}
