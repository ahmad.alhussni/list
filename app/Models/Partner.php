<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{

    protected $fillable = [
        'name',
        'image',
        'url',
        'status',
    ];

    public function getImageAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : "";
    }

    public function getCreatedAtAttribute($val)
    {
        if ($val != null) {
            $M = date("M", strtotime($val));
            $d = date("d", strtotime($val));
            $Y = date("Y", strtotime($val));
            return __($M) . ' ' . $d . ', ' . $Y;
        }

    }

}
