<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['user_id','coupon_id','shipment_id','payment_id'];

    function user(){
        return $this->belongsTo(User::class);
    }

    function details(){
        return $this->hasMany(OrderDetailes::class);
    }

    function shipment(){
        return $this->belongsTo(Shipment::class);
    }

    function payment(){
        return $this->belongsTo(Payment::class);
    }

}
