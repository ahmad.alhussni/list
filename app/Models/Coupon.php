<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{

    protected $fillable = [
        'code',
        'start_date',
        'end_date',
        'type',
        'amount',
        'status'
    ];

    public function getCreatedAtAttribute($val)
    {
        if ($val != null) {
            $M = date("M", strtotime($val));
            $d = date("d", strtotime($val));
            $Y = date("Y", strtotime($val));
            return __($M) . ' ' . $d . ', ' . $Y;
        }

    }

}
