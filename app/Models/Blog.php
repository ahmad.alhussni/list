<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $fillable = [
        'name',
        'description',
        'image',
        'slug',
        'status',
        'user_id',
        'category_id',
    ];

    public function getImageAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : "";
    }

    public function category(){
        return $this->belongsTo(Category::class );
    }

    function user(){
        return $this->belongsTo(User::class );
    }

    public function getCreatedAtAttribute($val)
    {
        if ($val != null) {
            $M = date("M", strtotime($val));
            $d = date("d", strtotime($val));
            $Y = date("Y", strtotime($val));
            return __($M) . ' ' . $d . ', ' . $Y;
        }

    }

}
