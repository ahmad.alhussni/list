<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Translation;

class Consultancy extends Model
{
    protected $fillable = ['image','description','user_id','status','slug','user_id'];
    protected $guarded = [];

    public function getImageAttribute($val)
    {
        return ($val != null) ? asset('storage/' . $val) : "";
    }

}
