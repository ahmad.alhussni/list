<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    protected $fillable = [
        'key','value','language'
    ];

    static public function translate($key,$language)
    {
        return ($translate = self::where('key',$key)->where('language',$language)->first()) ? $translate->value : "";
    }

}
