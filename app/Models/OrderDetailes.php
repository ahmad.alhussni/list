<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetailes extends Model
{
    protected $table = 'order_detailes';

    protected $guarded = [];


    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
