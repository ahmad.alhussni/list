<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::where('user_id',auth()->id())->with('details')->paginate(10);
        return response()->json(['message' => 'success', "data" => $orders]);
    }

    public function show($id)
    {
        $order = Order::where('user_id',auth()->id())->where('id',$id)->with('details')->first();
        return response()->json(['message' => 'success', "data" => $order]);
    }


}

