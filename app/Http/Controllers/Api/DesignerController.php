<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;

class DesignerController extends Controller
{

    public function index()
    {

        $role = Role::where('name',"Designer")->first();
        if(!$role){
            return response()->json(['message' => 'not exist']);
        }

        $users = $role->users;
        return response()->json(['message' => 'success', "data" => $users]);

    }

    public function show($id)
    {

        $role = Role::where('name',"Designer")->first();
        if(!$role){
            return response()->json(['message' => 'not exist']);
        }

        $user = $role->users->where('id',$id)->first();
        if(!$user){
            return response()->json(['message' => 'not exist']);
        }

        return response()->json(['message' => 'success', "data" => $user]);

    }


}

