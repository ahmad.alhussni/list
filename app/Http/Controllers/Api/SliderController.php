<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Slider;

class SliderController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $sliders = Slider::query();
        if(request('type')){
            $sliders = $sliders->where('type',request('type'));
        }
        $sliders = $sliders->get();
        return response()->json(['message' => 'success', "data" => $sliders]);

    }
}
