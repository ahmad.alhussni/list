<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Rate;

class RateController extends Controller
{

    public function index()
    {
        $rates = Rate::paginate();

        return response()->json(['message' => 'success', "data" => $rates]);

    }

    public function store()
    {
        $rates = request()->validate([
            'value' => 'required|in:1,2,3,4,5',
            'comment' => 'required',
        ]);

        $rates['parent_type'] = "App";
        $rates['parent_id'] = 0;

        $rate = Rate::create($rates);

        return response()->json(['message' => 'success', "data" => $rate]);
    }

}

