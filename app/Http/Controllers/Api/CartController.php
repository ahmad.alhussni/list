<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderDetailes;
use App\Models\Payment;
use App\Models\Shipment;

class CartController extends Controller
{

    public function index()
    {

        $projects = Cart::where('user_id', auth()->id())->get();

        return response()->json(['message' => 'success', "data" => $projects]);

    }

    public function store()
    {

        $validations = request()->validate([
            "parent_id" => "required",
            "parent_type" => "required",
            "qty" => "required",
        ]);

        $validations['user_id'] = auth()->id();

        $cart = Cart::where("user_id", auth()->id())->where('parent_id', request('parent_id'))->where('parent_type', request('parent_type'))->first();

        if ($cart) {
            if($cart->qty + request('qty') >= 0 ) {
                if($cart->qty + request('qty') == 0){
                    $cart->delete();
                }else{
                    $cart->qty = $cart->qty + request('qty');
                    $cart->save();
                }
            }
        } else {
            $cart = Cart::create($validations);
        }

        return response()->json(['message' => 'success', "data" => $cart]);

    }

    public function confirm()
    {

        $carts = Cart::where('user_id', auth()->id())->get();

        $coupon = Coupon::where('code', request('code'))->first();
        $coupon_id = 0;
        if ($coupon) {
            $coupon_id = $coupon->id;
        }

        $payment = Payment::where('id', request('payment_id'))->first();
        $payment_id = 0;
        if ($payment) {
            $payment_id = $payment->id;
        }

        $shipment = Shipment::where('id', request('shipment_id'))->first();
        $shipment_id = 0;
        if ($shipment) {
            $shipment_id = $shipment->id;
        }

        $order = new Order();
        $order->user_id = auth()->id();
        $order->coupon_id = $coupon_id;
        $order->shipment_id = $shipment_id;
        $order->payment_id = $payment_id;
        $order->note = request('note')?request('note'):"";
        $order->save();

        foreach ($carts as $cart) {
            $OrderDetailes = OrderDetailes::query();
            $OrderDetailes->parent_id = $cart->parent_id;
            $OrderDetailes->parent_type = $cart->parent_type;
            $OrderDetailes->qty = $cart->qty;
            $OrderDetailes->order_id = $order->id;
        }

        return response()->json(['message' => 'success', "data" => $order]);

    }


}

