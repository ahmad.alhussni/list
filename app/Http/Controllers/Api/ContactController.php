<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Contact;

class ContactController extends Controller
{

    public function index()
    {
        return Category::where('type','contact')->get();
    }

    public function store()
    {
        $contacts = request()->validate([
            'category_id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'title' => 'required',
            'message' => 'required'
        ]);

        $contact = Contact::create($contacts);

        return response()->json(['message' => 'success', "data" => $contact]);
    }

}

