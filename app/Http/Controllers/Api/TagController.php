<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Tag;

class TagController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $tags = Tag::query();
        if(request('type')){
            $tags = $tags->where('type',request('type'));
        }
        $tags = $tags->get();
        return response()->json(['message' => 'success', "data" => $tags]);

    }
}
