<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function registerPost(Request $request)
    {
        $validate = $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'mobile' => 'required|unique:users,mobile',
            'email' => '',
            'image' => 'image',
            'password' => 'required',
            'note' => '',
            'terms' => 'accepted',
        ]);

        $validate['image'] = $request->image ? $request->image->store('courses') : "images/logo.png";
        $validate['password'] = Hash::make($request->password);
        $user = User::create($validate);


        $user->syncRoles("Customers");

        if (!$user->token) {
            $token = $user->createToken('API');
            $user->token = $token->plainTextToken;
            $user->save();
        }

        return response()->json(['message' => 'success', "data" => $user, 'token' => $user->token]);

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginPost(Request $request)
    {
        request()->validate([
            'mobile' => 'required|exists:users',
            'password' => 'required|min:6',
        ]);

        $credentials = request()->only('mobile', 'password');
        if (auth()->attempt($credentials)) {
            $user = User::where('id', auth()->id())->first();
            if (!$user->token) {
                $token = $user->createToken('API');
                $user->token = $token->plainTextToken;
                $user->save();
            }
            return response()->json(['message' => 'success', "data" => $user, 'token' => $user->token]);
        }
        return response()->json(['message' => __("Error in username or password")]);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPasswordPost(Request $request)
    {
        request()->validate([
            'mobile' => 'required|exists:users',
        ]);

        $user = User::where('mobile', request('mobile'))->first();
        $user->code = 123456; //rand(100000,999999);
        $user->save();
        //send password here by SMS

        return response()->json(['message' => "success", "data" => $user]);
    }

    public
    function resetPasswordConfirmPost(Request $request)
    {

        request()->validate([
            'mobile' => 'required|exists:users',
            'password' => 'required|min:6|confirmed',
        ]);


        $user = User::where('id', request('mobile'))->first();
        if ($user->code != $request->code) {
            return response()->json(['message' => __("Error code")]);
        }

        $user->password = Hash::make($request->password);
        $user->save();


        return response()->json(['message' => "success", 'data' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function profile()
    {
        $user = User::findOrFail(auth()->id());
        return response()->json(['message' => 'success', "data" => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function profilePost(Request $request)
    {
        $user = User::findOrFail(auth()->id());

        $validateArray = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'mobile' => 'required|unique:users,mobile,' . $user->id,
            'email' => '',
            'image' => 'image',
            'note' => '',

        ];

        if ($request->has('password')) {
            $validateArray['password'] = 'required|min:6|confirmed';
        }

        $validate = $request->validate($validateArray);

        if ($request->has('first_name')) {
            $validate['first_name'] = $request->first_name;
        }
        if ($request->has('last_name')) {
            $validate['last_name'] = $request->last_name;
        }
        if ($request->has('mobile')) {
            $validate['mobile'] = $request->mobile;
        }
        if ($request->has('email')) {
            $validate['email'] = $request->email;
        }
        if ($request->has('image')) {
            $validate['image'] = $request->image->store('users');
        }
        if ($request->has('password')) {
            $validate['password'] = Hash::make($request->password);
        }
        if ($request->has('note')) {
            $validate['note'] = $request->note;
        }
        if ($request->has('status')) {
            $validate['status'] = $request->status;
        }
        $user->update($validate);

        if (!$user->token) {
            $token = $user->createToken('API');
            $user->token = $token->plainTextToken;
            $user->save();
        }

        return response()->json(['message' => 'success', "data" => $user, "token" => $user->token]);
    }
}
