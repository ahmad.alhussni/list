<?php

namespace App\Http\Controllers\Contractor;

use App\Http\Controllers\Controller;
use App\Models\Setting;

use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function dashboard()
    {

        if (request('lang')) {
            Session::put('lang', request('lang'));
            app()->setLocale(request('lang'));
            return redirect()->back();
        }

        return view('contractor.index');
    }
}
