<?php

namespace App\Http\Controllers\Contractor;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function login()
    {
        return view('contractor.login');
    }

    public function loginPost()
    {

        //dd(Hash::make(request('password')));
        if (auth()->attempt(['email' => request()->input('email'), 'password' => request()->input('password')])) {

            if(auth()->user()->hasRole('Contractors')){
                return redirect(route('contractor.home'));
            }
            Auth::logout();
        }

        return redirect()->back()->with(['error' => 'خطأ في اسم المستخد او كلة المرور']);
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        } else {
            return redirect()->back();
        }

    }

}

