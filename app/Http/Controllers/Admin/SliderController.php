<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:sliders management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Slider::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.sliders.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|array',
            //'description' => 'required|array',
            'image' => 'required|image',
            'url' => 'required'
        ]);


        $item = Slider::create([
            'image' => $request->image->store('sliders'),
            'url' => request('url'),
            'type' => request('type')
        ]);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }

 
        if(isset($request['description']) && !empty($request['description']))
        {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'description',
                    ],
                    [
                        'value' => request('description')[$locale] ? request('description')[$locale] : ""
                    ]
                );

            }
        }

         return redirect()->route('admin.sliders.index')->with(['success' => __("Slider created")]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Slider::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json(['item' => $item]);
        } else {
            return view('admin.sliders.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'image' => 'image'
        ]);


        $item = Slider::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('sliders');
        }
        if ($request->has('url')) {
            $data['url'] = $request->url;
        }
        if ($request->status >= 0) {
            $data['status'] = $request->status;
        }

        if ($request->has('type')) {
            $data['type'] = $request->type;
        }

        $item->update($data);

        if (request('name')) {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'name',
                    ],
                    [
                        'value' => request('name')[$locale] ? request('name')[$locale] : ""
                    ]
                );

            }
        }

        if (request('description')) {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'description',
                    ],
                    [
                        'value' => request('description')[$locale] ? request('description')[$locale] : ""
                    ]
                );

            }
        }

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.sliders.index')->with(['success' => __("Slider updated")]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::where('id', $id)->delete();

        return redirect()->route('admin.sliders.index')->with(['success' => __("Slider deleted")]);
    }

}
