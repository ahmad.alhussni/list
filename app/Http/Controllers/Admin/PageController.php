<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:pages management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Page::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.pages.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|array',
            'description' => 'required|array',
            'image' => 'image'
        ]);


        $item =  Page::create([
            'image' => $request->image ? $request->image->store('pages') : NULL,
            'slug' => Str::slug(request('name')['en'])
        ]);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }


        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'description',
                ],
                [
                    'value' => request('description')[$locale] ? request('description')[$locale] : ""
                ]
            );

        }

        return redirect()->route('admin.pages.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Page::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json(['item' => $item]);
        } else {
            return view('admin.pages.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'image' => 'image'
        ]);


        $item = Page::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('pages');
        }
        if ($request->status >= 0) {
            $data['status'] = $request->status;
        }
        $item->update($data);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }


        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'description',
                ],
                [
                    'value' => request('description')[$locale] ? request('description')[$locale] : ""
                ]
            );

        }


        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.pages.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::where('id',$id)->delete();

        return redirect()->route('admin.pages.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
