<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:products management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Product::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.products.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validations = $request->validate([
            "image" => "required|image",
            "name" => "required",
            "price" => "required",
            "category_id" => "required",
            "new" => "",
            "best_seller" => "",
            "best_price" => "",
            "color" => "required",
            "material_id" => "required",
        ]);


        if($request->image){
            $validations['image'] = $request->image->store('products');
        }

        $validations['user_id'] = $request->image->store('user_id');

        $item = Product::create($validations);

        return redirect()->route('admin.products.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Product::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json(['item' => $item]);
        } else {
            return view('admin.products.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Product::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('products');
        }
        if ($request->has('name')) {
            $data['name'] = $request->name;
        }

        if ($request->has('category_id')) {
            $data['category_id'] = $request->category_id;
        }
        if ($request->has('new')) {
            $data['new'] = $request->new ? 1 : 0;
        }
        if ($request->has('best_seller')) {
            $data['best_seller'] = $request->best_seller ? 1 : 0;
        }
        if ($request->has('best_price')) {
            $data['best_price'] = $request->best_price ? 1 : 0;
        }
        if ($request->has('color')) {
            $data['color'] = $request->color;
        }
        if ($request->has('material_id')) {
            $data['material_id'] = $request->material_id;
        }
        if ($request->status >= 0) {
            $data['status'] = $request->status;
        }
        $item->update($data);

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.products.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->delete();

        return redirect()->route('admin.products.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
