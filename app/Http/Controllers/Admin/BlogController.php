<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:blogs management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Blog::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.blogs.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $categories = Category::get();
        return view('admin.blogs.add',compact('users','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'image' => 'image',
            'category_id' => 'required|exists:categories,id',
            'user_id' => 'required|exists:users,id',
        ]);

        $item =  Blog::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $request->image ? $request->image->store('blogs') : "images/logo.png",
            'slug' => Str::slug(request('name')),
            'user_id' => $request->user_id,
            'category_id' => $request->category_id,
        ]);

        return redirect()->route('admin.blogs.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Blog::findOrFail($id);
        $users = User::get();
        $categories = Category::get();

        if (request()->is('api/*')) {
            return response()->json(['item' => $item,'users' => $users,'categories' => $categories]);
        } else {
            return view('admin.blogs.edit', compact('item','users','categories'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Blog::findOrFail($id);
        if ($request->has('name')) {
            $data['name'] = $request->name;
        }

        if ($request->has('description')) {
            $data['description'] = $request->description;
        }

        if ($request->has('image')) {
            $data['image'] = $request->image->store('blogs');
        }
        if ($request->has('user_id')) {
            $data['user_id'] = $request->user_id;
        }
        if ($request->has('category_id')) {
            $data['category_id'] = $request->category_id;
        }
        if ($request->status >= 0) {
            $data['status'] = $request->status;
        }
        $item->update($data);

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.blogs.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::where('id',$id)->delete();
        return redirect()->route('admin.blogs.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
