<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function login()
    {
        return view('admin.login');
    }

    public function loginPost()
    {
        
        Auth::loginUsingId(1);
        return redirect(route('admin.home'));
        //dd(Hash::make(request('password')));
        // if (auth()->attempt(['email' => request()->input('email'), 'password' => request()->input('password')])) {

        //     if(auth()->user()->hasRole('Admin')){
        //         return redirect(route('admin.home'));
        //     }
        //     Auth::logout();

        // }

        // return redirect()->back()->with(['error' => 'خطأ في اسم المستخد او كلة المرور']);
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        } else {
            return redirect()->back();
        }

    }

}

