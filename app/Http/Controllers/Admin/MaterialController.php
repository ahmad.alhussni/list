<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:materials management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Material::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.materials.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.materials.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|array',
            //'description' => 'required|array',
            'image' => 'required|image',
            'url' => 'required'
        ]);


        $item = Material::create([
            'image' => $request->image->store('materials'),
            'url' => request('url'),
            'type' => request('type')
        ]);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }


        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'description',
                ],
                [
                    'value' => request('description')[$locale] ? request('description')[$locale] : ""
                ]
            );

        }

        return redirect()->route('admin.materials.index')->with(['success' => __("Material created")]);


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Material::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json(['item' => $item]);
        } else {
            return view('admin.materials.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'image' => 'image'
        ]);


        $item = Material::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('materials');
        }
        if ($request->has('url')) {
            $data['url'] = $request->url;
        }
        if ($request->status >= 0) {
            $data['status'] = $request->status;
        }

        if ($request->has('type')) {
            $data['type'] = $request->type;
        }

        $item->update($data);

        if (request('name')) {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'name',
                    ],
                    [
                        'value' => request('name')[$locale] ? request('name')[$locale] : ""
                    ]
                );

            }
        }

        if (request('description')) {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'description',
                    ],
                    [
                        'value' => request('description')[$locale] ? request('description')[$locale] : ""
                    ]
                );

            }
        }

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.materials.index')->with(['success' => __("Material updated")]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Material::where('id', $id)->delete();

        return redirect()->route('admin.materials.index')->with(['success' => __("Material deleted")]);
    }

}
