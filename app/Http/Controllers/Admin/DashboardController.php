<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;

use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function dashboard()
    {

        if (request('lang')) {
            Session::put('lang', request('lang'));
            app()->setLocale(request('lang'));
            return redirect()->back();
        }

        return view('admin.index');
    }

    public function settings()
    {

        $settings = [
            'logo' => 'image',
            'admin_logo' => 'image',
            'footer_logo' => 'image',
            'name' => 'text',
            'description' => 'textarea',
            'email' => 'email',
            'facebook' => 'text',
            'twitter' => 'text',
            'instagram' => 'text',
            'google' => 'text',
            'phone' => 'text',
            'address' => 'text'
        ];

        return view('admin.settings', compact('settings'));
    }

    public function settingsPost()
    {

        $validates = request()->validate([
            'logo' => 'array',
            'admin_logo' => 'array',
            'footer_logo' => 'array',
            'name' => 'array',
            'description' => 'array',
            'email' => 'array',
            'facebook' => 'array',
            'twitter' => 'array',
            'instagram' => 'array',
            'google' => 'array',
            'image' => 'array',
            'phone' => 'array',
            'address' => 'array',
        ]);

        foreach ($validates as $key => $setting) {
            foreach (config('app.locales') as $locale) {

                if (!empty($setting[$locale]) and $setting[$locale] and !is_string($setting[$locale]) and $setting[$locale]->getType() == "file") {
                    Setting::updateOrCreate(
                        [
                            'language' => $locale,
                            'key' => $key,
                        ],
                        [
                            'value' => $setting[$locale] ? $setting[$locale]->store('logos') : Setting::translate($key,$locale)
                        ]
                    );
                } else {

                    Setting::updateOrCreate(
                        [
                            'language' => $locale,
                            'key' => $key,
                        ],
                        [
                            'value' => (!empty($setting[$locale]) and $setting[$locale]) ? $setting[$locale] : Setting::translate($key,$locale)
                        ]
                    );
                }
            }
        }

        return redirect()->back()->with('success', __("Settings updated"));
    }
}
