<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:projects management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Project::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.projects.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validations = $request->validate([
            "image" => 'required',
            "name" => 'required',
            "description" => 'required',
            "video_360" => 'required',
            "category_id" => 'required',
            "tag_id" => 'required',
        ]);

        if($request->image){
            $validations['image'] = $request->image->store('projects');
        }

        $validations['user_id'] = $request->image->store('user_id');

        $item = Project::create($validations);

        return redirect()->route('admin.projects.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Project::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json(['item' => $item]);
        } else {
            return view('admin.projects.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Project::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('projects');
        }
        if ($request->has('name')) {
            $data['name'] = $request->name;
        }
        if ($request->has('description')) {
            $data['description'] = $request->description;
        }
        if ($request->has('video_360')) {
            $data['video_360'] = $request->video_360;
        }
        if ($request->has('category_id')) {
            $data['category_id'] = $request->category_id;
        }
        if ($request->has('tag_id')) {
            $data['tag_id'] = $request->tag_id;
        }
        if ($request->status >= 0) {
            $data['status'] = $request->status;
        }

        $item->update($data);

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.projects.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::where('id', $id)->delete();

        return redirect()->route('admin.projects.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
