<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:categories management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Category::query();
        if(request('category_id')){
            $items = $items->where('category_id',request('category_id'));
        }else{
            $items = $items->where('category_id',0);
        }
        $items = $items->paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.categories.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('category_id',0)->get();
        return view('admin.categories.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|array',
            'name.*' => 'required',
            'description' => 'required|array',
            'description.*' => 'required',
            'image' => 'image'
        ]);


        $item =  Category::create([
            'image' => $request->image?$request->image->store('categories'):"",
            'slug' => Category::where('slug',Str::slug(request('name')['en']))->count() > 0 ? Str::slug(request('name')['en']) . rand(0,999999) : Str::slug(request('name')['en']),
            'category_id' =>  $request->category_id
        ]);

        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'name',
                ],
                [
                    'value' => request('name')[$locale] ? request('name')[$locale] : ""
                ]
            );

        }


        foreach (config('app.locales') as $locale) {
            $item->translates()->updateOrCreate(
                [
                    'language' => $locale,
                    'key' => 'description',
                ],
                [
                    'value' => request('description')[$locale] ? request('description')[$locale] : ""
                ]
            );

        }

        return redirect()->route('admin.categories.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Category::findOrFail($id);
        $categories = Category::where('category_id',0)->get();

        if (request()->is('api/*')) {
            return response()->json(['item' => $item,'categories' => $categories]);
        } else {
            return view('admin.categories.edit', compact('item','categories'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            //'image' => 'required|image'
        ]);


        $item = Category::findOrFail($id);
        $data = [];
        if ($request->has('image')) {
            $data['image'] = $request->image->store('categories');
        }
        if ($request->has('category_id')) {
            $data['category_id'] = $request->category_id;
        }
        if ($request->has('status') and $request->status >= 0) {
            $data['status'] = $request->status;
        }
        $item->update($data);

        if(request('name')) {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'name',
                    ],
                    [
                        'value' => request('name')[$locale] ? request('name')[$locale] : ""
                    ]
                );

            }
        }

        if(request('description')) {
            foreach (config('app.locales') as $locale) {
                $item->translates()->updateOrCreate(
                    [
                        'language' => $locale,
                        'key' => 'description',
                    ],
                    [
                        'value' => request('description')[$locale] ? request('description')[$locale] : ""
                    ]
                );

            }
        }


        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.categories.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::where('id',$id)->delete();

        return redirect()->route('admin.categories.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
