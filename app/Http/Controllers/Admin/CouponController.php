<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CouponController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:coupons management']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Coupon::paginate(10);
        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.coupons.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $categories = Category::get();
        return view('admin.coupons.add',compact('users','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $validation =$request->validate([
            'code' => 'required',
            'start_date' => 'required|date:Y-m-d',
            'end_date' => 'required|date:Y-m-d',
            'type' => 'required',
            'amount' => 'required',
        ]);


        $item =  Coupon::create( $validation );

        return redirect()->route('admin.coupons.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Coupon::findOrFail($id);
        $users = User::get();
        $categories = Category::get();

        if (request()->is('api/*')) {
            return response()->json(['item' => $item,'users' => $users,'categories' => $categories]);
        } else {
            return view('admin.coupons.edit', compact('item','users','categories'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Coupon::findOrFail($id);

        if($request->has('code')) {
            $data['code'] = $request->code;
        }

        if($request->has('start_date')) {
            $data['start_date'] = $request->start_date;
        }

        if($request->has('end_date')) {
            $data['end_date'] = $request->end_date;
        }
        if($request->has('type')) {
            $data['type'] = $request->type;
        }
        if($request->has('amount')) {
            $data['amount'] = $request->amount;
        }
        if($request->status >= 0 ) {
            $data['status'] = $request->status;
        }

        $item->update($data);

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.coupons.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::where('id',$id)->delete();
        return redirect()->route('admin.coupons.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
