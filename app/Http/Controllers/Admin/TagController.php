<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = Tag::paginate(10);

        if (request()->is('api/*')) {
            return response()->json($items);
        } else {
            return view('admin.tags.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required|string|unique:tags',
        ]);

        Tag::create([
            'slug' => $request->slug,
        ]);

        return redirect()->route('admin.tags.index')->with(['success' => 'تم الحفظ بنجاح']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Tag::findOrFail($id);

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return view('admin.tags.edit', compact('item'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Tag::findOrFail($id);

        $data = [];

        if($request->has('slug')){
            $data['slug'] = $request->slug;
        }
        if($request->status or $request->status === "0"){
            $data['status'] = $request->status ? $request->status : 0;
        }
        $item->update($data);

        if (request()->is('api/*')) {
            return response()->json($item);
        } else {
            return redirect()->route('admin.tags.index')->with(['success' => 'تم ألتحديث بنجاح']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Tag::where('id',$id)->delete();

        return redirect()->route('admin.tags.index')->with(['success' => 'تم الحذف بنجاح']);
    }

}
