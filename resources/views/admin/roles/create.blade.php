@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('css')
    <style>

    </style>
@endsection


@section('page-header')
    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span
                        class="font-weight-semibold">{{__('Pages')}}</span>
                    - {{__('Create')}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> {{__('Home')}}</a>

                    <a href="{{route('admin.pages.index')}}" class="breadcrumb-item"><i
                            class="icon-grid6 mr-2"></i> {{__('Pages')}}</a>
                    <span class="breadcrumb-item active">{{__('Create')}}</span>
                </div>

            </div>

        </div>
    </div>
    <!-- /page header -->
@endsection

@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{__('Create roles')}}</h5>
        </div>

        <div class="card-body">
            <form action="{{route('admin.roles.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tab1">
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">{{__('Name')}}</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="tab-content">
                        <label class="col-form-label">{{__('Permissions')}}</label>
                        <div class="row">

                            @foreach(\App\Models\User::perms() as $key => $permission)
                                <div class="col-md-6">
                                    <div class="row">
                                            <div class="col-md-4">
                                                <label>
                                                    <input type="checkbox" name="permissions[]"
                                                           value="{{$permission}}">
                                                    {{__($permission)}}
                                                </label>
                                            </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i>
                        </button>
                    </div>
            </form>
        </div>
    </div>


@endsection


@section('js_code')
    <script>
        $('.addImage').click(function () {
            $('.imageInputs').append(`
             <div class="form-group row">
                    <label class="col-form-label col-lg-2">{{__('Images')}}</label>
                    <div class="col-lg-10">
                        <input type="file" class="form-control" name="image[]">
                    </div>
                </div>
            `)
        });
    </script>
@endsection
