@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('css')
    <style>

    </style>
@endsection


@section('page-header')
    <!-- Role header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">{{__('Home')}}</span>
                    - {{__('Roles')}}</h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item"><i
                            class="icon-home2 mr-2"></i> {{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__('Roles')}}</span>
                </div>

            </div>

        </div>
    </div>
    <!-- /page header -->
@endsection

@section('content')

    <a type="button" class="btn bg-success mb-2"
       href="{{route('admin.roles.create')}}">{{__('Add new role')}} <i
            class="icon-plus3 ml-2"></i></a>


    <!-- Basic table -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{__('Roles')}}</h5>
        </div>


        <form class="form-group row p-2">
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-md-4">
                        <span class="form-text text-muted text-center">{{__('Role name')}}</span>
                        <input type="text" name="page_name" class="form-control" value="{{request('role_name')}}">
                    </div>

                    <div class="col-md-4">
                        <span class="form-text text-muted text-center"> &nbsp;</span>
                        <input type="submit" value="Search" class="btn btn-success">
                    </div>
                </div>
            </div>
        </form>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('Permissions')}}</th>
                    <th>{{__('Users')}}</th>
                    <th>{{__('Action')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td>{{$role->id}}</td>
                        <td>{{$role->name}}</td>
                        <td> {{ $role->permissions->count() }} </td>
                        <td><a href="{{route('admin.users.index',['role_id' => $role->id ])}}">{{ $role->users->count() }}</a></td>
                        <td width="150">
                            <a class="btn btn-sm btn-info d-inline"
                               href="{{route('admin.roles.edit',$role->id)}}"><i
                                    class="icon-pencil7"></i></a>
                            <form method="POST" action="{{route('admin.roles.destroy',$role->id)}}"
                                  class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger"
                                        onclick="return confirm('Are you sure?')"><i class="icon-trash-alt"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>
    <!-- /basic table -->

@endsection


@section('js')

@endsection
