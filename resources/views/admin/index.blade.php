@extends('admin.layouts.app')
@section('title','Dashboard Admin')
@section('content')

    <!-- Quick stats boxes -->
    <div class="row">
        <div class="col-lg-4">

            <!-- Members online -->
            <div class="card bg-teal-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\User::count()}}</h3>
                     </div>

                    <div>
                        {{__("Furniture")}}
                    </div>
                </div>

                <div class="container-fluid">
                    <div id="members-online"></div>
                </div>
            </div>
            <!-- /members online -->

        </div>

        <div class="col-lg-4">

            <!-- Current server load -->
            <div class="card bg-pink-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\Order::where('status',"1")->count()}}</h3>
                    </div>

                    <div>
                        {{__("Designers")}}
                    </div>
                </div>

                <div id="server-load"></div>
            </div>
            <!-- /current server load -->

        </div>

        <div class="col-lg-4">

            <!-- Today's revenue -->
            <div class="card bg-orange-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\Order::sum('total')}}</h3>
                    </div>
                    <div>
                        {{__("Contractors")}}
                    </div>
                </div>

                <div id="today-revenue"></div>
            </div>
            <!-- /today's revenue -->

        </div>

        <div class="col-lg-4">

            <!-- Members online -->
            <div class="card bg-teal-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\User::count()}}</h3>
                     </div>

                    <div>
                        {{__("Order")}}
                    </div>
                </div>

                <div class="container-fluid">
                    <div id="members-online"></div>
                </div>
            </div>
            <!-- /members online -->

        </div>

        <div class="col-lg-4">

            <!-- Current server load -->
            <div class="card bg-pink-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\Order::where('status',"1")->count()}}</h3>
                    </div>

                    <div>
                        {{__("User")}}
                    </div>
                </div>

                <div id="server-load"></div>
            </div>
            <!-- /current server load -->

        </div>

        <div class="col-lg-4">

            <!-- Today's revenue -->
            <div class="card bg-orange-400">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0">{{\App\Models\Order::sum('total')}}</h3>
                    </div>
                    <div>
                        {{__("Visitor")}}
                    </div>
                </div>

                <div id="today-revenue"></div>
            </div>
            <!-- /today's revenue -->

        </div>
    </div>
    <!-- /quick stats boxes -->

    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Latest posts -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">{{__("Latest orders")}}</h6>
                </div>

                <div class="card-body pb-0">

                    @php
                        $items = \App\Models\Order::where('status',"0")->limit(10)->get();
                    @endphp

                    @if($items->count() > 0)
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Email')}}</th>
                                <th>{{__('Details')}}</th>
                                <th>{{__('Sub total')}}</th>
                                <th>{{__('Coupon')}}</th>
                                <th>{{__('Discount')}}</th>
                                <th>{{__('Shipment')}}</th>
                                <th>{{__('total')}}</th>
                                <th>{{__('Payment')}}</th>
                                <th>{{__('Message')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Control')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>{{$item->user?$item->user->name:""}}</td>
                                    <td>{{$item->user?$item->user->email:""}}</td>
                                    <td>
                                        <ul>
                                            @foreach($item->details as $detail)
                                                <li>{{ $detail->course ? $detail->course->name : $detail }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>{{$item->sub_total}} {{__('CurrencyCode')}}</td>
                                    <td>{{$item->code}}</td>
                                    <td>{{$item->discount}} {{__('CurrencyCode')}}</td>
                                    <td>{{$item->shipment?$item->shipment->name:""}}</td>
                                    <td>{{$item->total}} {{__('CurrencyCode')}}</td>
                                    <td>{{$item->payment?$item->payment->name:""}}</td>
                                    <td>{{$item->note}}</td>
                                    <td>
                                        @if($item->status == 2) {{__("Cancel")}} @elseif($item->status == 1 ) {{__("Approved")}} @else
                                            {{__("Pending")}} @endif
                                    </td>
                                    <td>

                                                <span class="dropdown"><button id="btnSearchDrop28" type="button"
                                                                               data-toggle="dropdown"
                                                                               aria-haspopup="true" aria-expanded="true"
                                                                               class="btn btn-primary dropdown-toggle dropdown-menu-right">
                                                        <i class="icon-cog"></i></button>
                                                <span aria-labelledby="btnSearchDrop28"
                                                      class="dropdown-menu mt-1 dropdown-menu-left">

                                                  <form action="{{route('admin.orders.active', $item->id)}}" method="post">
                                                         @csrf
                                                        <button href="#" class="dropdown-item block-page"><i
                                                                class="icon-check"></i>{{ $item->status == 0 ?  __('Approve') : __('Pending')}}</button>
                                                  </form>
                                                    <form method="post"
                                                          action="{{route('admin.orders.destroy',$item-> id)}}">
                                                    @csrf
                                                        @method('DELETE')
                                                    <button class="dropdown-item block-page"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                                    </form>
                                                        </span>
                                             </span>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>

                        <br><br>

                    @else
                        <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                    @endif
                </div>
            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /dashboard content -->

    <!-- Dashboard content -->
    <div class="row">
        <div class="col-xl-12">

            <!-- Latest posts -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title">{{__("Latest posts")}}</h6>
                </div>

                <div class="card-body pb-0">
                    <div class="row">

                        @php
                            $items = \App\Models\Blog::limit(10)->get();
                        @endphp

                        @if($items->count() > 0)
                            @foreach($items as $item)
                                <div class="col-xl-6">

                                    <div class="media flex-column flex-sm-row mt-0 mb-3">

                                        <div class="mr-sm-3 mb-2 mb-sm-0">
                                            <div class="card-img-actions">
                                                <a href="#">
                                                    <img src="{{$item->image}}" class="img-fluid img-preview rounded"
                                                         alt="{{$item->name}}">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="media-body">
                                            <h6 class="media-title"><a href="#">{{$item->name}}</a></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item"><i
                                                        class="icon-book-play mr-2"></i> {{$item->user?$item->user->name:""}}
                                                </li>
                                            </ul>
                                            {{Str::limit($item->description,40)}}
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        @else
                            <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /dashboard content -->




@endsection
