@extends('admin.layouts.app')
@section('title',__('Payments'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}} </a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('admin.payments.index')}}"> {{__('Payments')}} </a>
                        </li>
                        <li class="breadcrumb-item active"> {{__('Edit payment')}} - {{$item->name}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Edit payment')}}</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('admin.payments.update',$item->id)}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <div class="text-center">
                                            <img src="{{$item->image}}" class="image-edit-show"
                                                 alt="{{__('Payment Image')}} ">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>{{__('Payment image')}}</label>
                                        <label id="projectinput7" class="file center-block">
                                            <input type="file" id="file" name="image">
                                            <span class="file-custom"></span>
                                        </label>
                                        @error('image')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-body">

                                        <h4 class="form-section"><i
                                                class="ft-home"></i> {{__('Payment details')}} </h4>
                                        <div class="row">


                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control"
                                                           for="name">{{__('Payment name')}}</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="name" name="name"
                                                               class="form-control border-primary"
                                                               placeholder="{{__('Payment name')}}"
                                                               value="{{ $item->name}}">
                                                        @error('name')
                                                        <span
                                                            class="text-danger">{{$message}}</span>
                                                        @enderror

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control"
                                                           for="name">{{__('Payment description')}}</label>
                                                    <div class="col-md-9">
                                                        <textarea type="text" id="description" name="description"
                                                               class="form-control border-primary"
                                                                  placeholder="{{__('Payment description')}}">{{ $item->description}}</textarea>
                                                        @error('description')
                                                        <span
                                                            class="text-danger">{{$message}}</span>
                                                        @enderror

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
