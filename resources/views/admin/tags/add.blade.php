@extends('admin.layouts.app')
@section('title',__('Tags'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href=""> {{__('Tags')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Add tag')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Add tag')}}</h4>
                            <a class="heading-elements-toggle"><i
                                    class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form" action="{{route('admin.tags.store')}}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-body">

                                        <h4 class="form-section"><i
                                                class="ft-home"></i> {{__('Tag details')}} </h4>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="slug">{{__('Tag slug')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" id="slug"
                                                       name="slug"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Tag slug')}}"
                                                       value="{{ old('slug')}}">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
