@extends('admin.layouts.app')
@section('title',__('Users'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}} </a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('admin.users.index')}}"> {{__('Users')}} </a>
                        </li>
                        <li class="breadcrumb-item active"> {{__('Edit user')}} - {{$item->name}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Edit user')}}</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('admin.users.update',$item->id)}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="user-tab" data-toggle="tab" href="#user"
                                               role="tab" aria-controls="user" aria-selected="true">User details</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="company-tab" data-toggle="tab" href="#company"
                                               role="tab" aria-controls="company" aria-selected="false">Company details</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="user" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> {{__('User details')}}
                                                </h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="role_id">{{__('User role')}} </label>
                                                            <div class="col-md-9">
                                                                <img src="{{$item->image}}" width="150"
                                                                     alt="{{__('User Image')}} ">
                                                                <input type="file" id="file" name="image">
                                                            </div>
                                                            @error('image')
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>

{{--                                                    <div class="col-md-12">--}}
{{--                                                        <div class="form-group row">--}}
{{--                                                            <label class="col-md-3 label-control"--}}
{{--                                                                   for="role_id">{{__('User role')}} </label>--}}
{{--                                                            <div class="col-md-9">--}}
{{--                                                                <select type="text" id="role_id" name="role_id"--}}
{{--                                                                        class="form-control border-primary">--}}
{{--                                                                    <option></option>--}}
{{--                                                                    @foreach(\Spatie\Permission\Models\Role::get() as $role)--}}
{{--                                                                        <option--}}
{{--                                                                            {{ in_array($role->id,$item->roles->pluck('id')->toArray()) ? 'selected' : '' }} value="{{$role->id}}">{{$role->name}}</option>--}}
{{--                                                                    @endforeach--}}
{{--                                                                </select>--}}
{{--                                                                @error('role_id')--}}
{{--                                                                <span--}}
{{--                                                                    class="text-danger">{{$message}}</span>--}}
{{--                                                                @enderror--}}

{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="first_name">{{__('User first_name')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="first_name" name="first_name"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('User first_name')}}"
                                                                       value="{{ $item->first_name}}">
                                                                @error('first_name')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="last_name">{{__('User last_name')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="last_name" name="last_name"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('User last_name')}}"
                                                                       value="{{ $item->last_name}}">
                                                                @error('last_name')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="mobile">{{__('User mobile')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="mobile" id="mobile" name="mobile"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('User mobile')}}"
                                                                       value="{{ $item->mobile}}">
                                                                @error('mobile')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="email">{{__('User email')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="email" id="email" name="email"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('User email')}}"
                                                                       value="{{ $item->email}}">
                                                                @error('email')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="password">{{__('Password')}}</label>
                                                            <div class="col-md-9">

                                                                <input type="password" id="password" name="password"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Password')}}">

                                                                @error('password')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="note">{{__('User note')}}</label>
                                                            <div class="col-md-9">
                                                                <textarea type="text" id="note"
                                                                          name="note"
                                                                          class="form-control border-primary"
                                                                          placeholder="{{__('User note')}}">{{ $item->note}}</textarea>
                                                                @error('note')
                                                                <span
                                                                    class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="company" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <div class="form-body">

                                                <h4 class="form-section"><i
                                                        class="ft-home"></i> {{__('Company details')}}
                                                </h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="logo">{{__('Logo')}}</label>
                                                            <div class="col-md-9">
                                                                <img src="{{$item->logo}}" width="150">
                                                                <input type="file" id="logo" name="logo"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Logo')}}">
                                                                @error('logo')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="company_name">{{__('Company name')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="company_name"
                                                                       name="company_name"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Company name')}}"
                                                                       value="{{$item->company_name}}">
                                                                @error('company_name')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="company_description">{{__('Company description')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="company_description"
                                                                       name="company_description"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Company description')}}"
                                                                       value="{{$item->company_description}}">
                                                                @error('company_description')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="video_360">{{__('Video 360')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="video_360" name="video_360"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Video 360')}}"
                                                                       value="{{$item->video_360}}">
                                                                @error('video_360')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="delivery">{{__('Delivery')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="checkbox" id="delivery" name="delivery"
                                                                       placeholder="{{__('Delivery')}}"
                                                                       value="{{$item->delivery}}">
                                                                @error('delivery')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="phone">{{__('Phone')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="phone" name="phone"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Phone')}}"
                                                                       value="{{$item->phone}}">
                                                                @error('phone')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="whatsapp">{{__('Whatsapp')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="text" id="whatsapp" name="whatsapp"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Whatsapp')}}"
                                                                       value="{{$item->whatsapp}}">
                                                                @error('whatsapp')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="website">{{__('Website')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="url" id="website" name="website"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Website')}}"
                                                                       value="{{$item->website}}">
                                                                @error('website')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="facebook">{{__('Facebook')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="url" id="facebook" name="facebook"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Facebook')}}"
                                                                       value="{{$item->facebook}}">
                                                                @error('facebook')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"
                                                                   for="instagram">{{__('Instagram')}}</label>
                                                            <div class="col-md-9">
                                                                <input type="url" id="instagram" name="instagram"
                                                                       class="form-control border-primary"
                                                                       placeholder="{{__('Instagram')}}"
                                                                       value="{{$item->instagram}}">
                                                                @error('instagram')
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection
