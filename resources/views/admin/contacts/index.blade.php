@extends('admin.layouts.app')
@section('title',__('Contacts'))
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Contacts')}}</h4>
                    <a class="heading-elements-toggle"><i
                            class="icon-ellipsis-v font-medium-3"></i></a>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#modalPoll-1">
                            {{__("Send message")}}
                        </button>

                        <!-- Modal: modalPoll -->
                        <div class="modal fade right" id="modalPoll-1" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel"
                             aria-hidden="true" data-backdrop="false">
                            <div class="modal-dialog modal-full-height modal-right modal-notify modal-info"
                                 role="document">
                                <form method="post" class="modal-content">
                                {{csrf_field()}}

                                <!--Header-->
                                    <div class="modal-header">
                                        <p class="heading lead">Feedback request
                                        </p>

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" class="white-text">×</span>
                                        </button>
                                    </div>

                                    <!--Body-->
                                    <div class="modal-body">
                                        <div class="info-contact">
                                            <p class="wrap-input-name">
                                                <select class="form-control" type="text" id="user_id" name="user_id"
                                                        tabindex="1" required="required">
                                                    <option>{{__("Select user...")}}</option>
                                                    @foreach(\App\Models\User::get() as $user)
                                                        <option value="{{$user->id}}">{{$user->name}} ({{$user->email}})</option>
                                                    @endforeach
                                                </select>
                                            </p>
                                            <p class="wrap-input-email">
                                                <input class="form-control" type="text" id="title" name="title"
                                                       value="{{old('title')}}"
                                                       tabindex="3" required="required" placeholder="عنوان الرسالة">
                                            </p>
                                            <p class="wrap-input-messages">
                                                <textarea class="form-control" id="messages-contact" name="message"
                                                          tabindex="4"
                                                          placeholder="الرسالة" required>{{old('message')}}</textarea>
                                            </p>
                                        </div>

                                    </div>

                                    <!--Footer-->
                                    <div class="modal-footer justify-content-center">
                                        <button type="submit"
                                                class="btn btn-primary waves-effect waves-light">{{__("Send")}}
                                            <i class="fa fa-paper-plane ml-1"></i>
                                        </button>
                                        <a type="button" class="btn btn-outline-primary waves-effect"
                                           data-dismiss="modal">{{__("Cancel")}}</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Modal: modalPoll -->

                        @if($items->count() > 0)
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Title')}}</th>
                                    <th>{{__('Message')}}</th>
                                    <th>{{__('Control')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->message}}</td>
                                        <td>

                                                <span class="dropdown"><button id="btnSearchDrop28" type="button"
                                                                               data-toggle="dropdown"
                                                                               aria-haspopup="true" aria-expanded="true"
                                                                               class="btn btn-primary dropdown-toggle dropdown-menu-right">
                                                        <i class="icon-cog"></i></button>
                                                <span aria-labelledby="btnSearchDrop28"
                                                      class="dropdown-menu mt-1 dropdown-menu-left">

                                                  <form action="{{route('admin.pages.update', $item->id)}}" method="post">
                                                         @csrf
                                                      @method('put')
                                                      <input type="hidden" name="status" value="{{ $item->status == 0 ?  1 : 0}}">
                                                        <button href="#" class="dropdown-item block-page"><i
                                                                class="icon-check"></i>{{ $item->status == 0 ?  __('read') : __('pending')}}</button>
                                                  </form>
                                                    <form method="post"
                                                          action="{{route('admin.pages.destroy',$item-> id)}}">
                                                    @csrf
                                                        @method('DELETE')
                                                    <button class="dropdown-item block-page"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                                    </form>
                                                        </span>
                                             </span>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>

                            {{$items->links()}}

                        @else
                            <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


