<!DOCTYPE html>
<html lang="en" dir="{{app()->getLocale() == 'ar' ? 'rtl' : 'ltr'}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ ($site_name = App\Models\Setting::translate('name',app()->getLocale())) ? $site_name : env("APP_NAME") }} - @yield('title')</title>
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{asset('limitless/global_assets')}}/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="{{asset('limitless/'.(app()->getLocale() == 'ar' ? 'rtl' : 'ltr'))}}/css/bootstrap.min.css"
          rel="stylesheet" type="text/css">
    <link href="{{asset('limitless/'.(app()->getLocale() == 'ar' ? 'rtl' : 'ltr'))}}/css/bootstrap_limitless.min.css"
          rel="stylesheet" type="text/css">
    <link href="{{asset('limitless/'.(app()->getLocale() == 'ar' ? 'rtl' : 'ltr'))}}/css/layout.min.css"
          rel="stylesheet" type="text/css">
    <link href="{{asset('limitless/'.(app()->getLocale() == 'ar' ? 'rtl' : 'ltr'))}}/css/components.min.css"
          rel="stylesheet" type="text/css">
    <link href="{{asset('limitless/'.(app()->getLocale() == 'ar' ? 'rtl' : 'ltr'))}}/css/colors.min.css"
          rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    @if(app()->getLocale() == 'ar')
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
    @endif

<!-- Core JS files -->
    <script src="{{asset('limitless/global_assets')}}/js/main/jquery.min.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/main/bootstrap.bundle.min.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('limitless/global_assets')}}/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/plugins/ui/moment/moment.min.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/plugins/pickers/daterangepicker.js"></script>

    <script src="{{asset('limitless/'.(app()->getLocale() == 'ar' ? 'rtl' : 'ltr'))}}/js/app.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_pages/dashboard.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/streamgraph.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/sparklines.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/lines.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/areas.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/donuts.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/bars.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/progress.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/heatmaps.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/pies.js"></script>
    <script src="{{asset('limitless/global_assets')}}/js/demo_charts/pages/dashboard/light/bullets.js"></script>
    <!-- /theme JS files -->
    @yield('css')
</head>

<body>

@if(auth()->check() and auth()->user()->hasRole("Admin"))
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="logo-admin" style="width: 250px;">
            <a href="{{route('admin.home')}}" class="d-inline-block">
                <img height="50" src="/storage/{{App\Models\Setting::translate('logo',app()->getLocale())}}" alt="{{App\Models\Setting::translate('name',app()->getLocale())}}">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.settings')}}" class="navbar-nav-link caret-0">
                        <i class="icon-cog"></i> {{__("Settings")}}
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                        <i class="icon-flag3"></i> {{__(app()->getLocale())}}
                    </a>
                    <div class="dropdown-menu dropdown-content wmin-md-350">
                        <div class="dropdown-content-body dropdown-scrollable">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="mr-3">
                                        <a href="{{route('admin.home',['lang'=>'ar'])}}" class="text-green w-100"><i class="icon-flag3"></i> {{__("ar")}}</a>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="mr-3">
                                        <a href="{{route('admin.home',['lang'=>'en'])}}" class="text-blue w-100"><i class="icon-flag3"></i> {{__("en")}}</a>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </li>
            </ul>

            <span class="badge ml-md-3 mr-md-auto"> </span>

            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                       data-toggle="dropdown">
                        <img
                            src="{{ auth()->user()->image }}"
                            class="rounded-circle mr-2" height="34" alt="">
                        <span>{{ auth()->user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{route('admin.users.edit',auth()->user()->id)}}"
                           class="dropdown-item"><i class="icon-user-plus"></i> {{__("My profile")}}</a>
                        <a href="#" class="dropdown-item"><i class="icon-switch2"></i> {{__("Logout")}}</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->
@endif

<!-- Page content -->
<div class="page-content">

@if(auth()->check() and auth()->user()->hasRole("Admin"))

    <!-- Main sidebar -->
        <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-right8"></i>
                </a>
                {{__("Navigation")}}
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#"><img
                                        src="{{ auth()->user()->image }}"
                                        width="38" height="38" class="rounded-circle" alt=""></a>
                            </div>

                            <div class="media-body">
                                <div
                                    class="media-title font-weight-semibold">{{ auth()->user()->name }}</div>
                                <div class="font-size-xs opacity-50">
                                    <i class="icon-pin font-size-sm"></i>
                                    &nbsp;{{ auth()->user()->email }}
                                </div>
                            </div>

                            <div class="ml-3 align-self-center">
                                <a href="{{route('admin.users.edit', auth()->user()->id)}}"
                                   class="text-white"><i class="icon-cog3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <!-- Main -->
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">{{__("Main")}}</div>
                            <i class="icon-menu" title="Main"></i></li>

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin') ? 'active' : ''}}"
                               href="{{route('admin.home')}}">
                                <i class="icon-home4"></i>
                                <span class="menu-title">{{__('Dashboard')}} </span>
                            </a>
                        </li>

                        @if(auth()->user()->can(['roles management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/roles*') ? 'active' : ''}}"
                               href="{{route('admin.roles.index')}}">
                                <i class="icon-key"></i>
                                <span class="menu-title"> {{__('Roles')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['users management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/users*') ? 'active' : ''}}"
                               href="{{route('admin.users.index')}}">
                                <i class="icon-users4"></i>
                                <span class="menu-title"> {{__('Users')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['sliders management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/sliders*') ? 'active' : ''}}"
                               href="{{route('admin.sliders.index')}}">
                                <i class="fa fa-image"></i>
                                <span class="menu-title"> {{__('Sliders')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['pages management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/pages*') ? 'active' : ''}}"
                               href="{{route('admin.pages.index')}}">
                                <i class="fa fa-file-text-o"></i>
                                <span class="menu-title"> {{__('Pages')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['tags management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/tags*') ? 'active' : ''}}"
                               href="{{route('admin.tags.index')}}">
                                <i class="fa fa-tags"></i>
                                <span class="menu-title"> {{__('Tags')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['categories management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/categories*') ? 'active' : ''}}"
                               href="{{route('admin.categories.index')}}">
                                <i class="fa fa-list"></i>
                                <span class="menu-title"> {{__('Categories')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['blogs management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/blogs*') ? 'active' : ''}}"
                               href="{{route('admin.blogs.index')}}">
                                <i class="fa fa-newspaper-o"></i>
                                <span class="menu-title"> {{__('Blog')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['contacts management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/contacts*') ? 'active' : ''}}"
                               href="{{route('admin.contacts.index')}}">
                                <i class="icon-phone"></i>
                                <span class="menu-title"> {{__('Contacts')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['orders management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/orders*') ? 'active' : ''}}"
                               href="{{route('admin.orders.index')}}">
                                <i class="icon-paste2"></i>
                                <span class="menu-title"> {{__('Orders')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['partners management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/partners*') ? 'active' : ''}}"
                               href="{{route('admin.partners.index')}}">
                                <i class="icon-price-tags"></i>
                                <span class="menu-title"> {{__('Partners')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['payments management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/payments*') ? 'active' : ''}}"
                               href="{{route('admin.payments.index')}}">
                                <i class="fa fa-university"></i>
                                <span class="menu-title"> {{__('Payments')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['coupons management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/coupons*') ? 'active' : ''}}"
                               href="{{route('admin.coupons.index')}}">
                                <i class="fa fa-percent"></i>
                                <span class="menu-title"> {{__('Coupons')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['shipments management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/shipments*') ? 'active' : ''}}"
                               href="{{route('admin.shipments.index')}}">
                                <i class="fa fa-truck"></i>
                                <span class="menu-title"> {{__('Shipments')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['banners management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/banners*') ? 'active' : ''}}"
                               href="{{route('admin.banners.index')}}">
                                <i class="icon-eye8"></i>
                                <span class="menu-title"> {{__('Banners')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['projects management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/projects*') ? 'active' : ''}}"
                               href="{{route('admin.projects.index')}}">
                                <i class="fa fa-server"></i>
                                <span class="menu-title"> {{__('Projects')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['products management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/products*') ? 'active' : ''}}"
                               href="{{route('admin.products.index')}}">
                                <i class="fa fa-product-hunt"></i>
                                <span class="menu-title"> {{__('Products')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['consultancies management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/consultancies*') ? 'active' : ''}}"
                               href="{{route('admin.consultancies.index')}}">
                                <i class="fa fa-question-circle"></i>
                                <span class="menu-title"> {{__('Consultancies')}}</span>
                            </a>
                        </li>

                        @endif

                        @if(auth()->user()->can(['materials management']))

                        <li class="nav-item">
                            <a class="nav-link {{request()->is('admin/materials*') ? 'active' : ''}}"
                               href="{{route('admin.materials.index')}}">
                                <i class="fa fa-linode"></i>
                                <span class="menu-title"> {{__('Materials')}}</span>
                            </a>
                        </li>

                        @endif

                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /main sidebar -->

@endif

<!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

            @yield('content')

        </div>
        <!-- /content area -->

    @if(auth()->check() and auth()->user()->hasRole("Admin"))

        <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                            data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2020 - 2021. <a href="#">{{env('APP_NAME')}}</a> {{__("by")}} <a
                            href="https://www.alkmal.com/" target="_blank">kamal</a>
					</span>
                </div>
            </div>
            <!-- /footer -->

        @endif
    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    @if ($errors->any())

    Swal.fire({
        icon: 'error',
        title: 'خطأ...',
        text: '{{ $errors->first() }}',
    });

    @endif

    @if (session('success'))

    Swal.fire({
        icon: 'success',
        title: 'نجاح...',
        text: '{{ session('success') }}',
    });

    @endif

</script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js"></script>

<script>
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyBkGL423uTYJykhdZOaq8WD4piatnWz2so",
        authDomain: "ideco-7f03a.firebaseapp.com",
        databaseURL: "https://ideco-7f03a.firebaseio.com",
        projectId: "ideco-7f03a",
        storageBucket: "ideco-7f03a.appspot.com",
        messagingSenderId: "7861828507",
        appId: "1:7861828507:web:14bbbdba8d6089c336ff3e",
        measurementId: "G-TQHPC0B6ZB"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
</script>


@yield('js')
</body>
</html>
