@extends('admin.layouts.app')
@section('title',__('Categories'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}} </a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('admin.categories.index')}}"> {{__('Categories')}} </a>
                        </li>
                        <li class="breadcrumb-item active"> {{__('Edit category')}} - {{$item -> name}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Edit category')}}</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('admin.categories.update',$item -> id)}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <div class="text-center">
                                            <img src="{{$item->image}}" class="image-edit-show"
                                                 alt="{{__('Category Image')}} ">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>{{__('Category Image')}}</label>
                                        <label id="projectinput7" class="file center-block">
                                            <input type="file" id="file" name="image">
                                            <span class="file-custom"></span>
                                        </label>
                                        @error('image')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-body">

                                        <h4 class="form-section"><i class="ft-home"></i> {{__('Category Details')}}</h4>
                                        <div class="row">

                                            @foreach (config('app.locales') as $locale)
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control"
                                                                       for="name">{{__('Category name')}} @if(count(config('app.locales')) > 1)
                                                                        ({{__($locale)}}) @endif</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" id="name[{{$locale}}]"
                                                                           name="name[{{$locale}}]"
                                                                           class="form-control border-primary"
                                                                           placeholder="{{__('Category name')}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif"
                                                                           value="{{ $item->translate('name',$locale) }}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control"
                                                                       for="description-{{$locale}}">{{__('Category description')}} @if(count(config('app.locales')) > 1)
                                                                        ({{__($locale)}}) @endif</label>
                                                                <div class="col-md-9">
                                                                <textarea type="text" id="description-{{$locale}}"
                                                                          name="description[{{$locale}}]"
                                                                          class="form-control border-primary"
                                                                          placeholder="{{__('Category description')}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif">{{ $item->translate('description',$locale) }}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <p class="text-secondary">{{__("Select Parent Category")}}</p>
                                                <select class="select2-rtl form-control" name="category_id"
                                                        id="select2-rtl-multi">
                                                    <option value="0">{{__("Main category")}}</option>
                                                @if($categories && $categories->count() > 0)
                                                        @foreach($categories as $category)
                                                            <option
                                                                value="{{$category->id}}" {{ in_array($category->id,$category->categories->pluck('id')->toArray()) ? 'selected' : ''}}>{{$category->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
