@extends('admin.layouts.app')
@section('title',__('Coupons'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href=""> {{__('Coupons')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Add coupon')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Add page')}}</h4>
                            <a class="heading-elements-toggle"><i
                                    class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form class="form" action="{{route('admin.coupons.store')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf


                                        <div class="form-body">

                                            <h4 class="form-section"><i
                                                    class="ft-home"></i> {{__('Coupon details')}} </h4>
                                            <div class="row">


                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="code">{{__('Coupon code')}}</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="code" name="code"
                                                                   class="form-control border-primary"
                                                                   placeholder="{{__('Coupon code')}}"
                                                                   value="{{ old('code') ? old('code') : Str::random(10)}}">
                                                            @error('code')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="type">{{__('Coupon type')}}</label>
                                                        <div class="col-md-9">
                                                            <select type="text" id="type" name="type"
                                                                    class="form-control border-primary">
                                                                <option value="">{{__('Coupon type')}}</option>
                                                                <option
                                                                    {{ old('type') === 0 ? 'selected' : '' }} value="percent">{{__('Percent')}}</option>
                                                                <option
                                                                    {{ old('type') === 1 ? 'selected' : '' }} value="fixed">{{__('Fixed')}}</option>
                                                            </select>
                                                            @error('type')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="amount">{{__('Coupon amount')}}</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="amount" name="amount"
                                                                   class="form-control border-primary"
                                                                   placeholder="{{__('Coupon amount')}}"
                                                                   value="{{ old('amount')}}">
                                                            @error('amount')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror

                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="start_date">{{__('Coupon start date')}}</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="start_date" name="start_date"
                                                                   class="form-control border-primary datepicker"
                                                                   placeholder="{{__('Coupon start date')}}" >
                                                            @error('start_date')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="end_date">{{__('Coupon end date')}}</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="end_date" name="end_date"
                                                                   class="form-control border-primary datepicker"
                                                                   placeholder="{{__('Coupon end date')}}" >
                                                            @error('end_date')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn btn-warning mr-1 block-page"
                                                    onclick="history.back();">
                                                <i class="icon-cross"></i> {{__('Cancel')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary block-page">
                                                <i class="icon-check"></i> {{__('Submit')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

@endsection


@section('js')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
            });
        });

    </script>

@endsection
