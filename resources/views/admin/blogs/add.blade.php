@extends('admin.layouts.app')
@section('title',__('Blogs'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href=""> {{__('Blogs')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Add blog')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Add page')}}</h4>
                            <a class="heading-elements-toggle"><i
                                    class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form class="form" action="{{route('admin.blogs.store')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label> {{__('Blog image')}} </label>
                                            <label id="image" class="file center-block">
                                                <input type="file" id="image" name="image">
                                                <span class="file-custom"></span>
                                            </label>
                                            @error('image')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>

                                        <div class="form-body">

                                            <h4 class="form-section"><i
                                                    class="ft-home"></i> {{__('Blog details')}} </h4>
                                            <div class="row">


                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="name">{{__('Blog name')}}</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="name" name="name"
                                                                   class="form-control border-primary"
                                                                   placeholder="{{__('Blog name')}}"
                                                                   value="{{ old('name')}}">
                                                            @error('name')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="description">{{__('Blog description')}}</label>
                                                        <div class="col-md-9">
                                                                <textarea type="text" id="description"
                                                                          name="description"
                                                                          class="form-control border-primary"
                                                                          placeholder="{{__('Blog description')}}">{{ old('description')}}</textarea>
                                                            @error('description')
                                                            <span
                                                                class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="category_id">{{__('Category')}}</label>
                                                        <div class="col-md-9">
                                                            <select id="category_id" name="category_id"
                                                                    class="form-control border-primary">
                                                                <option value="">{{ __('Category')}}</option>
                                                                @foreach($categories as $category)
                                                                    <option
                                                                        value="{{ $category->id  }}" {{old('category_id') == $category->id ? 'selected' : ''}}>{{ $category->name  }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('category_id')
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control"
                                                               for="user_id">{{__('Writer')}}</label>
                                                        <div class="col-md-9">
                                                            <select id="user_id" name="user_id"
                                                                    class="form-control border-primary">
                                                                <option value="">{{ __('Writer')}}</option>
                                                                @foreach($users as $user)
                                                                    <option
                                                                        value="{{ $user->id  }}" {{old('user_id') == $user->id ? 'selected' : ''}}>{{ $user->name  }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('user_id')
                                                            <span class="text-danger">{{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="button" class="btn btn-warning mr-1 block-page"
                                                    onclick="history.back();">
                                                <i class="icon-cross"></i> {{__('Cancel')}}
                                            </button>
                                            <button type="submit" class="btn btn-primary block-page">
                                                <i class="icon-check"></i> {{__('Submit')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

@endsection


@section('js')
    <script src="//cdn.ckeditor.com/4.15.0/full/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
@endsection
