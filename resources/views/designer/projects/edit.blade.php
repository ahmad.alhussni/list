@extends('designer.layouts.app')
@section('title',__('Projects'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('designer.home')}}">{{__('Home')}} </a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('designer.projects.index')}}"> {{__('Projects')}} </a>
                        </li>
                        <li class="breadcrumb-item active"> {{__('Edit project')}} - {{$item -> name}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Edit project')}}</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('designer.projects.update',$item -> id)}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-body">

                                        <h4 class="form-section"><i class="ft-home"></i> {{__('Project Details')}}</h4>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="image">{{__('Project image')}}</label>
                                            <div class="col-md-9">
                                                <img src="{{$item->image}}" class="image-edit-show" width="150">

                                                <input type="file" id="image"
                                                       name="image"
                                                       placeholder="{{__('Project image')}}"
                                                       value="{{ $item->image }}">
                                                @error('image')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="name">{{__('Project name')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" id="name"
                                                       name="name"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Project name')}}"
                                                       value="{{ $item->name }}">
                                                @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="description">{{__('Project description')}}</label>
                                            <div class="col-md-9">
                                                <textarea type="text" id="description"
                                                       name="description"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Project description')}}"
                                                       rows="5">{{ $item->description }}</textarea>
                                                @error('description')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="video_360">{{__('Project video 360')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" id="video_360"
                                                       name="video_360"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Project video_360')}}"
                                                       value="{{ $item->video_360 }}">
                                                @error('video_360')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="category_id">{{__('Project category')}}</label>
                                            <div class="col-md-9">
                                                <select type="text" id="category_id" name="category_id" class="form-control border-primary">
                                                    <option value="">{{__('Project category')}}</option>
                                                    @foreach(\App\Models\Category::where('type','main')->get() as $category)
                                                        <option value="{{ $category->id  }}" {{$item->category_id == $category->id ? 'selected' : ''}}>{{ $category->name  }}</option>
                                                    @endforeach
                                                </select>
                                                @error('category_id')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="tag_id">{{__('Project tag')}}</label>
                                            <div class="col-md-9">
                                                <select type="text" id="tag_id" name="tag_id" class="form-control border-primary">
                                                    <option value="">{{__('Project tag')}}</option>
                                                    @foreach(\App\Models\Category::where('type','main')->get() as $tag)
                                                        <option value="{{ $tag->id  }}" {{$item->tag_id == $tag->id ? 'selected' : ''}}>{{ $tag->name  }}</option>
                                                    @endforeach
                                                </select>
                                                @error('tag_id')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
