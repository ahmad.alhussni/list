@extends('designer.layouts.app')
@section('title',__('Consultancies'))
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('designer.home')}}">{{__('Home')}} </a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('designer.consultancies.index')}}"> {{__('Consultancies')}} </a>
                        </li>
                        <li class="breadcrumb-item active"> {{__('Edit consultancy')}} - {{$item -> name}}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Edit consultancy')}}</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('designer.consultancies.update',$item -> id)}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-body">

                                        <h4 class="form-section"><i class="ft-home"></i> {{__('Consultancy Details')}}</h4>


                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="image">{{__('Product image')}}</label>
                                            <div class="col-md-9">
                                                <img src="{{$item->image}}" class="image-edit-show" width="150">
                                                <input type="file" id="image"
                                                       name="image"
                                                       placeholder="{{__('Product image')}}"
                                                       value="{{ $item->image }}">
                                                @error('image')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="name">{{__('Product name')}}</label>
                                            <div class="col-md-9">
                                                 <input type="text" id="name"
                                                       name="name"
                                                        class="form-control border-primary"
                                                       placeholder="{{__('Product name')}}"
                                                       value="{{ $item->name }}">
                                                @error('name')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="description">{{__('Product description')}}</label>
                                            <div class="col-md-9">
                                                 <textarea rows="5" id="description"
                                                       name="description"
                                                           class="form-control border-primary"

                                                       placeholder="{{__('Product description')}}">{{ $item->description }}</textarea>
                                                @error('description')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
