@extends('designer.layouts.app')
@section('title',__('Orders'))
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Orders')}}</h4>
                    <a class="heading-elements-toggle"><i
                            class="icon-ellipsis-v font-medium-3"></i></a>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        @if($items->count() > 0)
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Details')}}</th>
                                    <th>{{__('Sub total')}}</th>
                                    <th>{{__('Coupon')}}</th>
                                    <th>{{__('Discount')}}</th>
                                    <th>{{__('Shipment')}}</th>
                                    <th>{{__('total')}}</th>
                                    <th>{{__('Payment')}}</th>
                                    <th>{{__('Message')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Control')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->user?$item->user->name:""}}</td>
                                        <td>{{$item->user?$item->user->email:""}}</td>
                                        <td>
                                            <ul>
                                                @foreach($item->details as $detail)
                                                    <li>{{ $detail->course ? $detail->course->name : $detail }}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>{{$item->sub_total}} {{__('CurrencyCode')}}</td>
                                        <td>{{$item->code}}</td>
                                        <td>{{$item->discount}} {{__('CurrencyCode')}}</td>
                                        <td>{{$item->shipment?$item->shipment->name:""}}</td>
                                        <td>{{$item->total}} {{__('CurrencyCode')}}</td>
                                        <td>{{$item->payment?$item->payment->name:""}}</td>
                                        <td>{{$item->note}}</td>
                                        <td>
                                            @if($item->status == 2) {{__("Cancel")}} @elseif($item->status == 1 ) {{__("Approved")}} @else
                                                {{__("Pending")}} @endif
                                        </td>
                                        <td>

                                                <span class="dropdown"><button id="btnSearchDrop28" type="button"
                                                                               data-toggle="dropdown"
                                                                               aria-haspopup="true" aria-expanded="true"
                                                                               class="btn btn-primary dropdown-toggle dropdown-menu-right">
                                                        <i class="icon-cog"></i></button>
                                                <span aria-labelledby="btnSearchDrop28"
                                                      class="dropdown-menu mt-1 dropdown-menu-left">

                                                  <form action="{{route('designer.orders.active', $item->id)}}" method="post">
                                                         @csrf
                                                        <button href="#" class="dropdown-item block-page"><i
                                                                class="icon-check"></i>{{ $item->status == 0 ?  __('Approve') : __('Pending')}}</button>
                                                  </form>
                                                    <form method="post"
                                                          action="{{route('designer.orders.destroy',$item-> id)}}">
                                                    @csrf
                                                        @method('DELETE')
                                                    <button class="dropdown-item block-page"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                                    </form>
                                                        </span>
                                             </span>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>

                            {{$items->links()}}

                        @else
                            <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
