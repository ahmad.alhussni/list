@extends('contractor.layouts.app')
@section('title',__('Sliders'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('contractor.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href=""> {{__('Sliders')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Add slider')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Add slider')}}</h4>
                            <a class="heading-elements-toggle"><i
                                    class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form" action="{{route('contractor.sliders.store')}}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label> {{__('Slider image')}} </label>
                                        <label id="projectinput7" class="file center-block">
                                            <input type="file" id="image" name="image">
                                            <span class="file-custom"></span>
                                        </label>
                                        @error('image')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-body">

                                        <h4 class="form-section"><i
                                                class="ft-home"></i> {{__('Slider details')}} </h4>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="url">{{__('Slider url')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" id="url"
                                                       name="url"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Slider url')}}"
                                                       value="{{ old('url')}}">
                                            </div>
                                        </div>

                                        <div class="row">

                                            @foreach (config('app.locales') as $locale)
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control"
                                                                       for="name">{{__('Slider name')}} @if(count(config('app.locales')) > 1)
                                                                        ({{__($locale)}}) @endif</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" id="name[{{$locale}}]"
                                                                           name="name[{{$locale}}]"
                                                                           class="form-control border-primary"
                                                                           placeholder="{{__('Slider name')}} @if(count(config('app.locales')) > 1) ({{__($locale)}}) @endif"
                                                                           value="{{ old('name') ? old('name')[$locale] : '' }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
