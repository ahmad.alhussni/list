@extends('furniture.layouts.app')
@section('title',__('Services'))
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Services')}}</h4>
                    <a href="{{route('services.create')}}"
                       class="btn-sm btn-info block-page float-right">
                        {{__('Add service')}}
                    </a>
                </div>

                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        @if($items->count() > 0)
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Control')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->status == 1 ? __('Active') : __('Inactive')}}</td>
                                        <td>
                                            <img style="width: 150px; height: 100px;"
                                                 src="{{$item->image}}"></td>
                                        <td>

                                                <span class="dropdown"><button id="btnSearchDrop28" type="button"
                                                                               data-toggle="dropdown"
                                                                               aria-haspopup="true" aria-expanded="true"
                                                                               class="btn btn-primary dropdown-toggle dropdown-menu-right">
                                                        <i class="icon-cog"></i></button>
                                                <span aria-labelledby="btnSearchDrop28"
                                                      class="dropdown-menu mt-1 dropdown-menu-left">
                                            <a href="{{route('services.edit',$item-> id)}}"
                                               class="dropdown-item block-page">
                                                <i class="icon-pencil"></i> {{__('Edit')}}</a>
                                                  <form action="{{route('services.active', $item->id)}}" method="post">
                                                         @csrf
                                                        <button href="#" class="dropdown-item block-page"><i
                                                                class="icon-check"></i>{{ $item->status == 0 ?  __('Active') : __('Inactive')}}</button>
                                                  </form>
                                                    <form method="post"
                                                          action="{{route('services.destroy',$item-> id)}}">
                                                    @csrf
                                                        @method('DELETE')
                                                    <button class="dropdown-item block-page"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                                    </form>
                                                        </span>
                                             </span>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>

                            {{$items->links()}}

                        @else
                            <h4 class="alert alert-danger text-center"> {{__('There is no items')}}</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
