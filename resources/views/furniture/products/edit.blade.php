@extends('furniture.layouts.app')
@section('title',__('Products'))
@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('furniture.home')}}">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item"><a href=""> {{__('Products')}}</a></li>
                        <li class="breadcrumb-item active"> {{__('Edit product')}} - {{$item->name}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic form layout section start -->
        <section id="basic-form-layouts">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> {{__('Add product')}}</h4>
                            <a class="heading-elements-toggle"><i
                                    class="icon-ellipsis-v font-medium-3"></i></a>
                        </div>

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <form class="form"
                                      action="{{route('furniture.products.update',$item -> id)}}"
                                      method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-body">

                                        <h4 class="form-section"><i
                                                class="ft-home"></i> {{__('Product details')}} </h4>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="image">{{__('Product image')}}</label>
                                            <div class="col-md-9">
                                                <img src="{{$item->image}}" class="image-edit-show" width="150">

                                                <input type="file" id="image"
                                                       name="image"
                                                       placeholder="{{__('Product image')}}"
                                                       value="{{ $item->image }}">
                                                @error('image')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="name">{{__('Product name')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" id="name"
                                                       name="name"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Product name')}}"
                                                       value="{{ $item->name }}">
                                                @error('name')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="price">{{__('Product price')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" id="price"
                                                       name="price"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Product price')}}"
                                                       value="{{ $item->price }}">
                                                @error('price')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="category_id">{{__('Product category')}}</label>
                                            <div class="col-md-9">
                                                <select type="text" id="category_id" name="category_id" class="form-control border-primary">
                                                    <option value="">{{__('Product category')}}</option>
                                                    @foreach(\App\Models\Category::where('type','main')->get() as $category)
                                                        <option value="{{ $category->id  }}" {{$item->category_id == $category->id ? 'selected' : ''}}>{{ $category->name  }}</option>
                                                    @endforeach
                                                </select>
                                                @error('category_id')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror

                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="new">{{__('Product new')}}</label>
                                            <div class="col-md-9">
                                                <input type="checkbox" id="new"
                                                       name="new"
                                                       placeholder="{{__('Product new')}}"
                                                       value="1"
                                                    {{ $item->new ? 'checked' : '' }}>
                                                @error('new')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="best_seller">{{__('Product best_seller')}}</label>
                                            <div class="col-md-9">
                                                <input type="checkbox" id="best_seller"
                                                       name="best_seller"
                                                       placeholder="{{__('Product best_seller')}}"
                                                       value="1"
                                                    {{ $item->best_seller ? 'checked' : '' }}>
                                                @error('best_seller')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="best_price">{{__('Product best price')}}</label>
                                            <div class="col-md-9">
                                                <input type="checkbox" id="best_price"
                                                       name="best_price"
                                                       placeholder="{{__('Product best price')}}"
                                                       value="1"
                                                    {{ $item->best_price ? 'checked' : '' }}>
                                                @error('best_price')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="color">{{__('Product color')}}</label>
                                            <div class="col-md-9">
                                                <input type="color" id="color"
                                                       name="color"
                                                       class="form-control border-primary"
                                                       placeholder="{{__('Product color')}}"
                                                       value="{{ $item->color }}">
                                                @error('color')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control"
                                                   for="material_id">{{__('Product material')}}</label>
                                            <div class="col-md-9">
                                                <select type="text" id="material_id" name="material_id" class="form-control border-primary">
                                                    <option value="">{{__('Product material')}}</option>
                                                    @foreach(\App\Models\Material::get() as $material)
                                                        <option value="{{ $material->id  }}" {{$item->material_id == $material->id ? 'selected' : ''}}>{{ $material->name  }}</option>
                                                    @endforeach
                                                </select>
                                                @error('material_id')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1 block-page"
                                                onclick="history.back();">
                                            <i class="icon-cross"></i> {{__('Cancel')}}
                                        </button>
                                        <button type="submit" class="btn btn-primary block-page">
                                            <i class="icon-check"></i> {{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
